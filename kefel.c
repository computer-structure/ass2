#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int checkNum(int k);

int power(int x, int y);

int main(int argc, char* argv[]){
    int k = atoi(argv[1]);

    FILE* fp;
    fp = fopen("kefel.s", "w,r");
    if (fp == NULL){
        printf("Error! can't open file");
        exit(1);
    }

    fprintf(fp, ".section .text\n.global kefel\nkefel: ");

    int isNeg = 0;

    if (k == 0 || k == 1 || k == -1){
        switch (k){
            case 0:
                fprintf(fp, "movl %%edi, %%eax\nsubl %%eax, %%eax\nret\n");
                break;
            case 1:
                fprintf(fp, "movl %%edi, %%eax\nret\n");
                break;
            case -1:
                fprintf(fp, "movl %%edi, %%eax\nneg eax\nret\n");
                break;
        }
    }
    else {
        if (k < 0) {
            isNeg = 1;
            k *= -1;
        }

        if (k == 3 || k == 5 || k == 9) {
            fprintf(fp, "movl %%edi, %%eax\nleal (%%eax, %%eax, %d), %%eax\nret\n", k - 1);
        }
        else {
            if (checkNum(k) > 0) {
                int exp = checkNum(k);
                int divider = k / power(2, checkNum(k)) - 1;
                if (checkNum(k) > 0) {
                    fprintf(fp, "movl %%edi, %%eax\nleal (%%eax, %%eax, %d), %%eax\n"
                                "shl $%d, %%eax\nret\n", divider, exp);
                }
            }
            else {

            }

        }
    }

    fclose(fp);
}

int checkNum(int k){
    if ((k % 3 == 0) || (k % 5 == 0) || (k % 9 == 0)){
        int temp = k;
        if (temp % 9 == 0)
            temp /= 9;
        if (temp % 5 == 0)
            temp /= 5;
        if (temp % 3 == 0)
            temp /= 3;
        int count = 0;
        while (temp % 2 == 0){
            temp /= 2;
            count++;
        }
        if (temp == 1)
            return count;
    }
    return 0;
}

int power(int x, int y){
    int temp = 1;
    int i = 0;
    for (i = 0; i < y; i++){
        temp *= x;
    }
    return temp;
}